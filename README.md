##Environment set up:
https://docs.openstack.org/infra/jenkins-job-builder/installation.html

Install python

`brew install python `

Install pip

`sudo easy_install pip`

Add pip packages to your path (.bash_profile or equivalent)

`export PATH="/Users/your_user/Library/Python/2.7/bin:$PATH"`

## Configuration file
```
[job_builder]
ignore_cache=True
keep_descriptions=False
include_path=.:scripts:~/git/
recursive=False
exclude=.*:manual:./development
allow_duplicates=False

[jenkins]
user=your_jenkins_username
password=your_jenkins_api_token
url=http://192.168.200.57:8090/
query_plugins_info=False
```
The api token can be found in API Token section at:
<http://192.168.200.57:8090/user/(your_jenkins_user)/configure>

##Jenkins Job Creation:

1.	Create a yaml file for a new job. An existing job's yaml file can be used as a template.

2. Modify the yaml file to suit your needs.

3. Test it by running:

`jenkins-jobs --conf jjb_config test <your_new_job>.yml`

4. Update Jenkins server:

`jenkins-jobs --conf jjb_config update <your_new_job>.yml`

5. Commit and push yaml file of the new job to respiri-jjb Bitbucket repository.

Further job definitions to create job yaml:

https://docs.openstack.org/infra/jenkins-job-builder/definition.html

##Respiri Jenkins Plugins required

- Cucumber reports

- Email Extension Plugin

## Configuring email when 2FA authentication is on for Gmail

Create a custom app in you Gmail security settings.

1. Log-in into Gmail with your account
2. Navigate to https://security.google.com/settings/security/apppasswords
3. In 'select app' choose 'custom', give it an arbitrary name and press generate
4. It will give you 16 chars token.

Use the token as password in combination with your full Gmail account and two factor authentication will not be required.

